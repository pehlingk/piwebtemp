
#set up with crontab so start at boot and restart every hour
#@reboot python2 /home/pi/repo/piwebtemp/webtemp.py
#1 * * * * /home/pi/repo/piwebtemp/webtemp.py
from flask import Flask, render_template
import RPi.GPIO as GPIO
import datetime
import sys
import time
import Adafruit_DHT
import numbers

app = Flask(__name__)
sensor = Adafruit_DHT.AM2302
pin = 4


humidity, temperature = Adafruit_DHT.read_retry(sensor,pin)
#   print(temperature)
     
temp = float("{:0.1f}".format(temperature))
hum = "{:0.1f}%".format(humidity)
Ftemp = temp * 9/5 + 32

@app.route('/')
def Hello():
    
    now = datetime.datetime.now()
    timeString = now.strftime("%m-%d %H:%M")
    templateData = {
        'title' : 'HELLO!',
        'time': timeString,
        'temp': Ftemp  
        }
    
    return render_template('main.html', **templateData)




if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0')
    
